package com.puntoB;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

public class Server {

    public Server(int port) {
        /*
        Capa de Trasnporte: TCP / UDP
         */
        try{
            ServerSocket ss = new ServerSocket(port);
            System.out.println("Server escuchando en el puerto " + port);
        
            while (true){
                Socket client = ss.accept();
                ServerHilo sh = new ServerHilo(client);
                Thread serverThread = new Thread(sh);
                serverThread.start();
                serverThread.join();

            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }
    public static void main(String[] args) {
        int port = Integer.valueOf(System.getenv("PUERTO_SERVIDOR"));     
        Server server = new Server(port);
    }
}
